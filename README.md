# DEVICE CI

## update-ci template

With Pantavisor device CI, you will be able to keep both your Pantahub devices updated and create flashable images from the state of those devices in an automated way, using GitLab CI capabilities and with a minimal configuration process. This tracing over the defined devices is performed both over their bsp and apps.

Device CI is aimed on maintaining one repository per project. That means multiple devices of the same project can be configured for multiple targets. For instance, we can have several devices which apps are automatically updated each day and several stable devices that are updated when a tag is created over a certain known and tested revision.

### Import our GitLab CI yml

To begin using it, just add this template to your new project as a submodule:

```
git submodule add https://gitlab.com/pantacor/ci/device-ci.git
```

After that, you will need to execute our mkdevice-ci.sh script from your newly added submodule, with the init operator and while in the root of your project:

```
./device-ci/mkdevice-ci.sh init
```

This operation will create a new device-ci.conf with default values. You can modify this file to fit your project needs:

```
# Device CI Configuration
yml_template="update-ci.yml"
device_list="device-daily-1:device-stable-1 device-daily-2:device-stable-2"
```

* yml_template. The yml template we are going to use for our CI. Only change this if you know what you are doing.
* device_list. A list of pair devices. First value defines the device that is updated when the project CI is triggered or scheduled. The second one defines the stable device that is promoted from a given state of the first value when tagged.

*NOTE:* All device names use the name assigned to the device in Pantahub. This name can be edited in the device panel on the web.

After editing the config file, we must execute again mkdevice-ci.sh with the install operator to make our changes effective:

```
./device-ci/mkdevice-ci.sh install
```

This will automatically generate the .gitlab-ci.yml file used by GitLab to execute its CI pipelines.

*IMPORTANT:* Do not forget to commit and push these changes to your GitLab repo so the pipeline can start effectively!

### Set up gitlab CI variables

In your gitlab project, go to _Settings / CI/CD / Variables_, and set these keys:

```
*PHUSER:* Pantahub user that owns the to-be-updated devices.
*PHPASS:* Pantahub password.
*PH_CIBOT_GITLAB_TOKEN:* GitLab access token for ci bot
*PH_CIBOT_GITLAB_USER:* gitlab user for ci bot
*AWS_ACCESS_KEY_ID:* Amazon AWS access ID.
*AWS_SECRET_ACCESS_KEY:* Amazon AWS secret key.
*AWS_BUCKET:* Amazon AWS bucket name.
*AWS_PROJECT_PATH:* Amazon AWS project path.
*DEPLOY_TRIGGER_PROJECT*: Project that is going to be triggered after a stable build is passed.
*DEPLOY_TRIGGER_TOKEN*: GitLan token with triggering permissions over the deploy project.
```

*IMPORTANT*: Don't forget to set your variables to Masked so they can not be seen in the GitLab log!

### Schedule your periodic job

To make your _scheduled_ devices self-updatable, a scheduling pipeling has to be configured. In you gitlab project, go to _CI/CD / Schedules_ and create a new schedule with the desired interval.

## pvdeps-ci template

With pvdeps-ci, you will be able to build the pantavisor binary when applying any change to a pantavisor dependency repository. Default jobs will build pantavisor for the -generic targets with your last commit included. Furthermore, it is possible to configure it so your changes are automatically posted to a Pantahub device.

### Import our GitLab CI yml

To start using the pvdeps-ci template, just import it in your project:

```
include:
  project: 'pantacor/ci/device-ci'
  ref: '011'
  file: '/yml/pvdeps-ci.yml'
```

### Set up gitlab CI variables

The CI will start working as is, generating the tarball artifacts with the pantavisor binary inside. If you want to automatically post your changes to one or several devices, you will have to set these variables from your GitLab project UI:

```
*PHUSER:* Pantahub user that owns the to-be-updated devices.
*PHPASS:* Pantahub password.
*PH_TARGET_DEVICE:* Pantahub device name. Several can be set separated by space character.
*ARTIFACT:* Name of the -generic target you want to use in your device. Several can be set separated by space character. The order of the artifacts must correspont to the order in PH_TARGET_DEVICE.
```

*IMPORTANT*: Don't forget to set your variables to Masked so they can not be seen in the GitLab log!

## bsp-ci template

This CI is meant to work in the [pv-manifest](https://gitlab.com/pantacor/pv-manifest) project, so it will need a couple of repo manifests: default.xml and release.xml.

It works with an upgrade job that will be triggered either form the GitLab scheduler or the GitLab UI. This job will check if there is any change in release.xml according to default.xml. If a change is detected, it will update release.xml with an automatic commit. This commit will in turn trigger a set of jobs that will build the Pantavisor BSP for the configured platforms and post the results to configurable Pantahub devices.

### Import our GitLab CI yml and configure your devices

To start using the bsp-ci template, just import it in your project:

```
include:
  project: 'pantacor/ci/device-ci'
  ref: '011'
  file: '/yml/bsp-ci.yml'
```

To configure the targets for the build jobs, you just need to configure these parameters for each target:

```
<job-name>:
  extends: .build-bsp
  variables:
    PLATFORM: <platform>
    TARGET: <target>
    PH_TARGET_DEVICE_LATEST: <pantahub-latest-device>
    PH_TARGET_DEVICE_STABLE: <pantahub-stable-device>
```

* job-name: Choose any unique name for your job.
* platform: Build platform.
* target: Build target.
* pantahub-latest-device: Pantahub device that will be updated from the GitLab scheduler or when a new commit is pushed.
* pantahub-stable-device: Pantahub device that will be updated when a new tag is pushed.

Example:

```
build-rpi3:
  extends: .build-bsp
  variables:
    PLATFORM: rpi3
    TARGET: arm-rpi3
    PH_TARGET_DEVICE_LATEST: arm_rpi3_bsp_latest
    PH_TARGET_DEVICE_STABLE: arm_rpi3_bsp_stable
```

### Set up gitlab CI variables

This CI will need Pantahub credentials to post the output BSP into the configured devices:

```
*PHUSER:* Pantahub user that owns the to-be-updated devices.
*PHPASS:* Pantahub password.
```

### Schedule your periodic job  

To make your _scheduled_ devices self-updatable, a scheduling pipeling has to be configured. In you gitlab project, go to _CI/CD / Schedules_ and create a new schedule with the desired interval.
