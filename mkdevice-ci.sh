#!/bin/bash

set -e

dir=`sh -c "cd $(dirname $0); pwd"`
modname=`basename $dir`
GITHEAD=${GITHEAD:-`cat $dir/../.git/modules/$modname/HEAD | sed -e 's/.*heads\///'`}

help_message() {
echo "Usage:"
			echo "mkdocker.sh <operation> [options]"
			echo ""
			echo "  init       initialize project: create device-ci.conf.json"
			echo "  install    install projcet: generate gitlab-ci.yml using the contents of device-ci.conf.json"
}

# parse arguments
if [ $# -gt 0 ] && [ $# -lt 3 ] ; then
    op="$1"
	case "$op" in
		("init")
			;;
		("install")
			;;
		(*)
			help_message
			exit 1
			;;
	esac
else
	help_message
	exit 0
fi

# execute operations
case "$op" in
	("init")
		cat > device-ci.conf.json << EOF1
{
  "yml_template":"update-ci.yml",
  "device_list": [
    {   
      "id":"rpi3_initial",
      "schedule":"rpi3_initial_latest",
      "tag":"rpi3_initial_stable",
      "target_overwrite": "",
      "build": [
        {
          "name":"default",
          "options":"PANTAVISOR_DEBUG=yes"
        }
      ]
    },
    {
      "id":"malta_initial",
      "schedule":"malta_initial_latest",
      "tag":"malta_initial_stable",
      "target_overwrite": "",
      "build": [
        {   
          "name":"default",
          "options":"PANTAVISOR_DEBUG=yes"
        }   
      ]   
    }
  ]       
}
EOF1
		exit 0
		;;
	("install")
		# load configuration
		if [ -f device-ci.conf.json ]; then
			yml_template=$(cat device-ci.conf.json | jq -r .yml_template)
			device_list=$(cat device-ci.conf.json | jq .device_list)
		else
			echo "error: device-ci.conf.json not found. Initialize project first"
			exit 1
		fi

        # take sha current submodule
        git add device-ci
        submodule_status=`git submodule status | grep device-ci`
        submodule_status_array=($submodule_status)
        submodule_sha=${GITHEAD}
        if test "$GITHEAD" == "__sha__"; then
                submodule_sha=${submodule_status_array[0]}
        fi

		# write ci include
		cat > .gitlab-ci.yml << EOF2
include:
  project: 'pantacor/ci/device-ci'
  ref: ${submodule_sha}
  file: '/yml/${yml_template}'

EOF2

		echo $device_list | jq -r .[].id | while read device; do
			schedule=$(echo $device_list | jq -r --arg de "$device" '.[] | select(.id==$de).schedule')
			tag=$(echo $device_list | jq -r --arg de "$device" '.[] | select(.id==$de).tag')
			subtarget=
set -x
			if test `echo $device_list | jq -c --arg de "$device" '.[] | select(.id==$de).subtarget==null'` = "false"; then
				subtarget=$(echo $device_list | jq -r -c --arg de "$device" '.[] | select(.id==$de).subtarget')
			fi
			altrepogroups=
			if test `echo $device_list | jq -c --arg de "$device" '.[] | select(.id==$de).altrepogroups==null'` = "false"; then
				altrepogroups=$(echo $device_list | jq -r -c --arg de "$device" '.[] | select(.id==$de).altrepogroups')
			fi
			build=$(echo $device_list | jq -c --arg de "$device" '.[] | select(.id==$de).build')
			build_escape="${build//\"/\\\"}"

			cat >> .gitlab-ci.yml << EOF3

update-$schedule:
  extends: .update-scheduled
  variables:
    PH_SCHEDULED_DEVICE: "$schedule"

check-$schedule:
  extends: .check-status-pushed
  variables:
    PH_TARGET_DEVICE: "$schedule"

build-$tag:
  extends: .build-stable
  variables:
    PH_SCHEDULED_DEVICE: "$schedule"
    PH_STABLE_DEVICE: "$tag"
    BUILD: "$build_escape"
    SUBTARGET: "$subtarget"
    ALTREPOGROUPS: "$altrepogroups"

check-$tag:
  extends: .check-status-stable
  variables:
    PH_TARGET_DEVICE: "$tag"
EOF3
		done
	;;
esac
