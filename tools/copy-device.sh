#!/bin/bash

set -e

help_message() {
echo "Usage:"
echo "  copy-device.sh <pvr-source> <revision-number> <pvr-target>"
}

# parse arguments
if [ $# -gt 0 ] && [ $# -lt 4 ] ; then
	pvrsource="$1"
	revnumber="$2"
	pvrtarget="$3"
else
	help_message
	exit 0
fi

# copy device
mkdir /tmp/pvr-copy; /tmp/pvr-copy

for i in $(seq 1 $revision-number); do
	pvr clone $pvrsource/$revnumber
	cd $i
	pvr post $pvrtarget
	cd ..
done

cd -;
